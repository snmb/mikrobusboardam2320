#include "main.h"
#include "dui.c"

#define AM2320_address (0xB8) 

unsigned int16 Humidity = 0;
signed int16 Temperature = 0;
int8 state = 0;
unsigned char data_buffer[8]; 

void divmodu10(int16 n, int16 *quot, int8 *rem);


//TASK MESSAGE CHECK
#task(rate = 200 ms, max = 300 us)
void task_check();

#task(rate = 10 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//-------------------------------------------------


void task_check() {
   switch (state) {
      case 0:
         output_bit(LED1, 1);
         //Wake_Sensor
         i2c_start();
         i2c_write(AM2320_address);
         i2c_stop(); 
         //request temperature and humidity
         i2c_start();
         i2c_write(AM2320_address);
         i2c_write(0x03);
         i2c_write(0x00);
         i2c_write(0x04);
         i2c_stop();
         output_bit(LED1, 0);
         state = 1;
         break;
      case 2:  
         output_bit(LED2, 1);
         i2c_start(); 
         i2c_write(AM2320_address | 0x01); 
         for (int8 i = 0; i < 7; i ++) data_buffer[i] = i2c_read(1);
         data_buffer[7] = i2c_read(0);
         i2c_stop();
         DUISendUInt8(2, data_buffer[0]);
        DUISendUInt8(3, data_buffer[1]);
        DUISendUInt8(4, data_buffer[2]);
        DUISendUInt8(5, data_buffer[3]);
        DUISendUInt8(6, data_buffer[4]);
        DUISendUInt8(7, data_buffer[5]);
        DUISendUInt8(8, data_buffer[6]); 
        DUISendUInt8(9, data_buffer[7]); 
         output_bit(LED2, 0);
         state = 3;
         break;
      case 3:  
         unsigned int16 crc = 0xFFFF; 
         unsigned int8 s = 0x00; 
         unsigned int8 len = 6;
         unsigned char *ptr = data_buffer;
         
         while( len -- ) { 
           crc ^= *ptr ++;
           for (s = 0; s < 8; s ++)  { 
             if ((crc & 0x01) != 0)  { 
               crc >>= 1; 
               crc ^= 0xA001; 
             } 
             else { 
               crc >>= 1; 
             } 
           } 
         } 

         output_bit(LED3, 0);
         if (crc ==  make16(data_buffer[7], data_buffer[6]))  state = 4;
         else state = 5;
         break;           
         
      case 4:
         output_bit(LED3, 1);
         Temperature = make16(data_buffer[4], data_buffer[5]);
         
         if (bit_test(Temperature,15)) {
            bit_clear(Temperature, 15);
            Temperature = -Temperature;
         }
         int16 quot;
         int8 rem;
         divmodu10(Temperature, &quot, &rem);
         Temperature = quot;
         quot = (int16)rem << 8;
         int16 ot;
         divmodu10(quot, &ot, &rem);

         
         
         DUISendUInt16(0, make16(data_buffer[2], data_buffer[3]));
         DUISendTemperature(1, Temperature, ot);

         state = 5;
         break;     
      case 10: state = 0;  break;      
      default: state ++; break;
   
   }
}

void divmodu10(int16 n, int16 *quot, int8 *rem) {
   int16 m = n;
   if (n > 32767) m = -n;
    *quot = m >> 1;
    *quot += *quot >> 1;
    *quot += *quot >> 4;
    *quot += *quot >> 8;
    int16 qq = *quot;
    *quot >>= 3;
     *rem = (int8)(m - ((*quot << 1) + (qq & ~(int16)7)));
    
    if (n > 32767) { 
        if(*rem > 10)  {
           *rem -= 10;
           *quot++;
        }
        *quot = ~*quot;
        *rem = ~*rem + 1;
        
    } else {
        if(*rem > 9)  {
           *rem -= 10;
           *quot++;
        }
    }
}

void task_DUIcheck() {
   DUICheck();
}

void formInit(void) {
   
   char text1[]="Humidity";
   char text2[]="%";
   DUIAddLabel(0, text1, text2);
   char text3[]="Temperature";
   char text4[]="C";
   DUIAddLabel(1, text3, text4);
   char suffix[]="";
   char textb0[]="b0";
    DUIAddLabel(2, textb0, suffix);   
   char textb1[]="b1";
    DUIAddLabel(3, textb1, suffix);
   char textb2[]="b2";
    DUIAddLabel(4, textb2, suffix);
   char textb3[]="b3";
    DUIAddLabel(5, textb3, suffix);   
   char textb4[]="b4";
    DUIAddLabel(6, textb4, suffix);
   char textb5[]="b5";
    DUIAddLabel(7, textb5, suffix);  
   char textb6[]="b6";
    DUIAddLabel(8, textb6, suffix);
   char textb7[]="b7";
    DUIAddLabel(9, textb7, suffix);

}

void formRead(unsigned int8 channel, unsigned int8* data) {

   if (channel == 4) {
   //   Temperature = DUIReadInt16(1, data);
   }

}

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x01 );
    SET_TRIS_C( 0x80 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);

}

void main() {
   initialization();
   rtos_run();
}
